=== The Authority ===

Contributors: yingling017
Tags: translation-ready, custom-background, theme-options, custom-menu, threaded-comments, editor-style, featured-images, one-column, two-column, right-sidebar, footer-widgets, custom-logo

Requires at least: 4.0
Tested up to: 4.8.1
Stable tag: 1.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A theme to establish your Authority on a topic.

== Description ==

The Authority is a lightning fast, responsive WordPress theme designed to make your content the center of attention. Activate the theme and get started sharing your authority with the world. Looking to release an ebook? No problem. The Authority supports Easy Digital Downloads so you can get started selling too.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

The Authority is designed to work with all plugins. There are a few plugins that The Authority
has customized support for including:

Easy Digital Downloads
Infinite Scroll in Jetpack
Divi Builder
Easy Footnotes
And many more

== Changelog ==

= 1.0.5 - October 6, 2017 =
Removing wpcom.php. Adding unminified mmenu. Escapting custom-header.php. Removing the_authority_admin_header_image() and the_authority_admin_header_style(). Returning excerpt and more content if in the admin screen.

= 1.0.4 - September 1, 2017 =
* Adding translatable text for read more and category / tag lists. Escaping data in title. Adding text domain to wpcom.php.

= 1.0.3 - August 23, 2017 =
* Fixing issue where customizer styles would not print out if header text color option wasn't changed

= 1.0.2 - August 23, 2017 =
* Removing Freemius. Updating styles for q and table elements. Mobile menu now closes automatically above 960px. Hamburger button controlled with Header Accent customizer setting.

= 1.0.1 =
* Minor updates

= 1.0 - May 11, 2017 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)

== Resources ==
* jquery.mmenu.min.js © Fred Heusschen, MIT
* Font Awesome Font License, SIL OFL 1.1
* Font Awesome Code License, MIT
* Bourbon © 2011-2015 thoughtbot, inc.
* Bourbon.Neat © 2011-2015 thoughtbot, inc.
